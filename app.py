#utf-8; python 3.6; flask app
# Phenomics annotation system 1
#/app.py
# Dror hilman 2018

from riotpy import *
from tags.navbar import *
from tags.tasks_tag import *


from router import *
from users import *
from file_uploader import *


    
    

class maintag(RiotTag):
    def HTML(self):
        navbar(title='LRM Files Conversion', base_color='black', logo='hazera_log_icon_small_white2.png')()
        RiotRouter(default_tag="tasks_tag")()

app = RiotPyApp(toptitle="Tracking to LRM file", 
                mount=['maintag'],
                body_style =  'background:#263238;',
                js_includes=["js/jquery-3.2.1.min.js","js/tree.jquery.js", "js/materialize.min.js", 'js/echarts.min.js', 'js/Drift.min.js', 'js/paper-full.min.js'],
                css_includes=["css/json-viewer.css","css/jqtree.css","css/drift-basic.min.css", "css/materialicons.css", "css/materialize.min.css", "favicon.ico"],
                blueprints=[usersBP,fileuploadBP]).app

'''@app.before_first_request
def load_dextr():
    import os, sys
    print('loading dxtr server')    

    cwd = os.getcwd()
    dex_wd = cwd + '/DEXTR_flask/'
    if '\\' in dex_wd: dex_wd = dex_wd.replace('/', '\\')
    cmd = sys.executable + ' ' + dex_wd + 'app.py'

    
    os.chdir(dex_wd)
    os.system(cmd)
    os.chdir(cwd)
  
    print('DEXTR started')'''
