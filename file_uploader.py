from flask import Blueprint, request, jsonify, session, render_template, url_for
import sys
import numpy as np
import re
import pandas as pd
from io import StringIO
import os
import shutil
import zipfile
from flask import Flask, flash, request, redirect, url_for,send_from_directory
from werkzeug.utils import secure_filename
from openpyxl import workbook
from openpyxl import load_workbook
from riotpy import *
fileuploadBP = Blueprint('fileuploadBP', __name__)
errormsg=''

def proc_tracking_2_LRM():
    tracking_fname='/myapp/temp_folder/infile.xlsm'
    tracking = pd.read_excel(tracking_fname, 'Samples_List',)
    # try:
    #     tracking = pd.read_excel(tracking_fname, 'Samples_List',)
    # except Exception as e:
    #     print(e)    

    #tracking = pd.read_excel(tracking_fname, 'Samples_List',)

    tracking_cols=['PLATE_NAME', 'PLATE_COORDINATES', 'GENOTYPE', 'LOT_CODE',
       'INTERNAL_CODE', 'BAG_OF_SEEDS_ID', 'SAMPLING_COMMENT', 'PEDIGREE',
       'START_CODE', 'GENERATION', 'CHECK', 'DNA_CONCENTRATION', 'DNA_VOLUME',
       'DNA_QUANTITY']
    #assert all(col in tracking.columns for col in tracking_cols), "Missing columns in tracking file.\nRequired columns are:" + str(tracking_cols) 
    assert tracking.columns.tolist() == tracking_cols, "Wrong columns in tracking file.\nRequired columns are:%s"%(str(tracking_cols))

    marker_techno = pd.read_excel(tracking_fname, 'Marker_Techno',)    
    
    lrm_df = pd.DataFrame(columns=['Lot code','Cultivar code','External sample Id','Generation','Start code','Number of samples','Start lot pedigree', 'SN / PN', 'Plant Nr'])
    # a column that contains a serial number of duplicate values to add to the EMPTY text
    tracking['dup_number'] = tracking.groupby(['GENOTYPE']).cumcount()+1
    lrm_df['dup_number'] = tracking['dup_number']
    # add the serial number ot the EMPTY text
    lrm_df['Lot code']=tracking.apply(lambda x:x['GENOTYPE'][:5]+str(x['dup_number']) if x['GENOTYPE'][:5]=='EMPTY' else x['GENOTYPE'],axis=1)

    lrm_df['Cultivar code'] = tracking['BAG_OF_SEEDS_ID']

    lrm_df['External sample Id'] = tracking['LOT_CODE']
    lrm_df['External sample Id']=lrm_df.apply(lambda x:x['External sample Id'][:5]+str(x['dup_number']) if x['External sample Id'][:5]=='EMPTY' else x['External sample Id'],axis=1)

    lrm_df['Number of samples'] = 1
    #lrm_df['Generation'] = 'Fn'

    # Split LOT_CODE in tracking file into plot and plant - plot goes to SN/PN in LRM file 
    lrm_df['SN / PN'] = tracking['LOT_CODE'].str.split("_").apply(lambda x: x[0])
    #lrm_df['SN / PN'] = tracking['LOT_CODE'].str.split("_").apply(lambda x: x[0] if len(x)>1  else x[0])
    # If there is EMPTY in the LOT_CODE in the tracking file it should be added a serial number in the LRM file
    #lrm_df['SN / PN']=lrm_df.apply(lambda x:x['SN / PN'][:5]+str(x['dup_number']) if x['SN / PN'][:5]=='EMPTY' else x['SN / PN'],axis=1)
    lrm_df['SN / PN']=lrm_df.apply(lambda x:'0' if x['SN / PN'][:5]=='EMPTY' else x['SN / PN'],axis=1)

    # Split LOT_CODE in tracking file into plot and plant - plant goes to Plant Nr in LRM file
    #lrm_df['Plant Nr'] = tracking['LOT_CODE'].str.split("_").apply(lambda x: x[1] if len(x)>1 else x[0])
    ######lrm_df['Plant Nr'] = tracking['LOT_CODE'].str.split("_").apply(lambda x: x[1] if len(x)>1 else '0')
    lrm_df['Plant Nr'] = tracking['LOT_CODE'].str.split("_").apply(lambda x: x[1] if len(x)>1 else '')
    # If there is EMPTY in the LOT_CODE in the tracking file it should be added a serial number in the LRM file
    #lrm_df['Plant Nr']=lrm_df.apply(lambda x:x['Plant Nr'][:5]+str(x['dup_number']) if x['Plant Nr'][:5]=='EMPTY' else x['Plant Nr'],axis=1)
    #lrm_df['Plant Nr']=lrm_df.apply(lambda x:str(x['dup_number']) if x['Plant Nr'][:5]=='EMPTY' else x['Plant Nr'],axis=1)
    lrm_df['Plant Nr']=lrm_df.apply(lambda x:str(x['dup_number']) if x['Lot code'][:5]=='EMPTY' else x['Plant Nr'],axis=1)
        
    lrm_df['Generation']=lrm_df['Lot code'].apply(lambda x: re.search('.+\-?(((F|X|C)\d\d)|(DHD)|(BC\dF\d))\-.+', x).group(1) if re.search('.+\-?(((F|X|C)\d\d)|(DHD)|(BC\dF\d))\-.+', x) else 'Fn')

    # drop the serial number column as it is not necessary anymore
    lrm_df.drop('dup_number', axis=1, inplace=True)
    
    # Data validity checks
    # Add an ERRORS column to report errors
    lrm_df['ERRORS']=''
    # Check that lot code is alphanumeric and length is max 30 and min (mandatory)
    lrm_df['ERRORS']+=lrm_df.apply(lambda x:'Lot code is mandatory; ' if str(x['Lot code'])=='nan'  else '',axis=1)
    lrm_df['ERRORS']+=lrm_df.apply(lambda x:'Lot code is not alphanumeric; ' if not bool(re.match('^[a-zA-Z0-9\-\_\/]+$', str(x['Lot code'])))  else '',axis=1)
    lrm_df['ERRORS']+=lrm_df.apply(lambda x:'Lot code length is longer than 30; ' if len(str(x['Lot code']))>30  else '',axis=1)
    # Check that cultivar is alphanumeric and length is max 30
    lrm_df['ERRORS']+=lrm_df.apply(lambda x:'Cultivar code is not alphanumeric; ' if not bool(re.match('^[a-zA-Z0-9\-]+$', str(x['Cultivar code'])))  else '',axis=1)
    lrm_df['ERRORS']+=lrm_df.apply(lambda x:'Cultivar code length is longer than 30; ' if len(str(x['Cultivar code']))>30  else '',axis=1)
    # External sample ID is mandatory
    lrm_df['ERRORS']+=lrm_df.apply(lambda x:'External sample Id is mandatory; ' if str(x['External sample Id'])=='nan'  else '',axis=1)
    # Generation is mandatory
    lrm_df['ERRORS']+=lrm_df.apply(lambda x:'Generation is mandatory; ' if str(x['Generation'])=='nan'  else '',axis=1)
    # Number of samples is mandatory
    lrm_df['ERRORS']+=lrm_df.apply(lambda x:'Number of samples is mandatory; ' if len(str(x['Number of samples']))==0  else '',axis=1)
    # Check that plot and plant are numeric only
    lrm_df['ERRORS']+=lrm_df.apply(lambda x:'SN/PN(plot) is not numeric; ' if not bool(re.match('^[0-9]+|EMPTY\d+$', str(x['SN / PN'])))  else '',axis=1)
    lrm_df['ERRORS']+=lrm_df.apply(lambda x:'Plant Nr is not numeric; ' if not bool(re.match('^[0-9]+|EMPTY\d+$', str(x['Plant Nr'])))  else '',axis=1)
    #print (bool(re.match('^[a-zA-Z0-9]+$', '123abc')))

    # Check if there are duplicated values of plot_plant
    lrm_df['is_duplicated'] = lrm_df.duplicated(['SN / PN', 'Plant Nr'])
    lrm_df['ERRORS']+=lrm_df.apply(lambda x:'plot_plant is duplicated; ' if x['is_duplicated'] else '',axis=1)
    #lrm_df[['is_duplicated', 'SN / PN', 'Plant Nr','ERRORS']]
    lrm_df.drop('is_duplicated', axis=1, inplace=True)
    # Get the content of cell B5 in the tracking file, datasheet 'marker_techno' and place it in cell A1 in the new LRM file
    lrm_df.loc[-3] = [marker_techno['NUMBER_OF_MARKERS'].values[3],'','','','','','','','','']  # adding a row
    if pd.isnull(marker_techno['NUMBER_OF_MARKERS'].values[3]):
        lrm_df.loc[-3] = ['','','','','','','','','','Missing markers (in Tracking file, sheet Marker Techno Cell B5)']

    # Add the text 'Plant to plant' to the second row of the LRM file
    lrm_df.loc[-2] = ['Tracking file','','','','','','','','','']  # adding a row
    lrm_df.loc[-1] = lrm_df.columns
    lrm_df.index = lrm_df.index + 3  # shifting index
    lrm_df.sort_index(inplace=True)
    writer = pd.ExcelWriter('/myapp/temp_folder/outfile.xlsx', engine='xlsxwriter', options={'border': 1})
    SheetName = 'Import Samples LRM'
    lrm_df.to_excel(writer, sheet_name=SheetName, startrow=0, header=False,index=False)
    workbook  = writer.book
    worksheet = writer.sheets[SheetName]
    border_format=workbook.add_format({'border':1})

    color_range2='A4:I'+str(lrm_df.shape[0])
    worksheet.conditional_format(color_range2, {'type': 'cell',
                                        'criteria': '>=',
                                        'value': 0, 'format': border_format})
    worksheet.conditional_format(color_range2, {'type': 'cell',
                                        'criteria': '<',
                                        'value': 0, 'format': border_format})  
    #worksheet.set_selection('A1:B20',12,border_format)

    
    cell_format = workbook.add_format({'bold': True, 'italic': True})
    worksheet.set_row(2,20,cell_format)

    cell_format = workbook.add_format({'bold': True, 'italic': True, 'font_color': 'red'})
    worksheet.set_column("J:J",20,cell_format)
    #worksheet.conditional_format(0, 0, 10, 5, {'type':     'cell',
    #                                          'criteria': '!=',
    #                                          'value':    'None',
    #                                          'format':   cell_format})

    writer.save()    

def proc_lrm_2_tracking():

    path = '/myapp/temp_folder/'
    tracking_fname = path+'trac_file.xlsm'
    lrm_fname = path+'lrm_file.xlsx'
    trac_df = pd.read_excel(tracking_fname , 'Samples_List')
    lrm_df = pd.read_excel(lrm_fname, skiprows=1)

    lrm_cols = ['Sample Code', 'SN/PN', 'Lot Code', 'Cultivar Code', 'Generation',
       'Start Code', 'Start Lot Pedigree', 'Start Cultivar Pedigree',
       'Plant Number', 'Plate code', 'Plate column', 'Plate row', 'Empty Well',
       'MET']
    assert lrm_df.columns.tolist() == lrm_cols,"Please choose a valid LRM file.\nRequired columns: %s"%(lrm_cols)

    trac_cols = ['PLATE_NAME', 'PLATE_COORDINATES', 'GENOTYPE', 'LOT_CODE',
       'INTERNAL_CODE', 'BAG_OF_SEEDS_ID', 'SAMPLING_COMMENT', 'PEDIGREE',
       'START_CODE', 'GENERATION', 'CHECK', 'DNA_CONCENTRATION', 'DNA_VOLUME',
       'DNA_QUANTITY']
    assert trac_df.columns.tolist() == trac_cols,"Please choose a valid Tracking file.\nRequired columns: %s"%(trac_cols)

    # Combine SN/PN and Plant Number in LRM to generate the key plot_plant - to look for in the tracking file
    lrm_df['SN/PN'].fillna(0,inplace=True)
    lrm_df['Plant Number'].fillna(0,inplace=True)
    lrm_df[['SN/PN', 'Plant Number']] = lrm_df[['SN/PN', 'Plant Number']].astype(int).astype(str)
    lrm_df['plot_plant'] = lrm_df['SN/PN']+'_'+lrm_df['Plant Number']

    ########################## CHANGED BY SHYULY 17-7-19 ############################################################
    lrm_df['plot_plant_lotcode'] = lrm_df['SN/PN']+'_'+lrm_df['Plant Number']+'_'+lrm_df['Lot Code']
    trac_df['INTERNAL_CODE_GENOTYPE'] = trac_df['INTERNAL_CODE']+'_'+trac_df['GENOTYPE']
    ##################################################################################################################

    # Check that all plot_plant keys in LRM file (SN/PN_Plant Number) exist in tracking file (INTERNAL_CODE)
    errmsg = ''

    ########################## CHANGED BY SHYULY 17-7-19 ############################################################
    # set_lrm_diff_trac = sorted(set(lrm_df['plot_plant']).difference(set(trac_df['INTERNAL_CODE'])))
    set_lrm_diff_trac = sorted(set(lrm_df['plot_plant_lotcode']).difference(set(trac_df['INTERNAL_CODE_GENOTYPE'])))
    ##################################################################################################################

    #set_lrm_diff_trac.remove('0_0')
    set_lrm_diff_trac = [x for x in set_lrm_diff_trac if "0_" not in x]
    #set_lrm_diff_trac.remove.str.contains('0_')
    if len(set_lrm_diff_trac) > 0:
        errmsg = 'Found SN/PN_Plant Number in LRM file that does not match INTERNAL_CODE in Tacking file:\n\n'
        for pp in set_lrm_diff_trac:
            #errmsg += 'SN/PN_Plant Number: ' + pp + '\n'
            errmsg += "SN/PN_Plant Number: %s\n"%(pp)

            ########################## CHANGED BY SHYULY 17-7-19 ############################################################
            # rows_list = np.array(lrm_df[lrm_df['plot_plant']==pp].index.tolist())+3
            rows_list = np.array(lrm_df[lrm_df['plot_plant_lotcode']==pp].index.tolist())+3
            ##################################################################################################################


            #errmsg += ' In rows: ' + np.array2string(rows_list, separator=',') + '\n\n'
            errmsg += "In rows: %s\n\n"%(np.array2string(rows_list, separator=','))

    # Check that all plot_plant keys in Tracking file (INTERNAL_CODE) exist in LRM file (SN/PN_Plant Number) 

    ########################## CHANGED BY SHYULY 17-7-19 ############################################################
    #set_trac_diff_lrm = sorted(set(trac_df['INTERNAL_CODE']).difference(set(lrm_df['plot_plant'])))
    set_trac_diff_lrm = sorted(set(trac_df['INTERNAL_CODE_GENOTYPE']).difference(set(lrm_df['plot_plant_lotcode'])))
    #set_trac_diff_lrm.remove('EMPTY')
    set_trac_diff_lrm.remove('EMPTY_EMPTY')
    ########################## CHANGED BY SHYULY 17-7-19 ############################################################

    if len(set_trac_diff_lrm) > 0:
        errmsg += 'Found INTERNAL_CODE in Tracking file that does not match SN/PN_Plant Number in LRM file:\n\n'
        for pp in set_trac_diff_lrm:
            errmsg += "INTERNAL_CODE: %s\n"%(pp)

            ########################## CHANGED BY SHYULY 17-7-19 ############################################################
            # rows_list = np.array(lrm_df[trac_df['INTERNAL_CODE']==pp].index.tolist())+2
            rows_list = np.array(lrm_df[trac_df['INTERNAL_CODE_GENOTYPE']==pp].index.tolist())+2
            ##################################################################################################################
            errmsg += "In rows: %s\n\n"%(np.array2string(rows_list, separator=',')) 

    assert errmsg == '', errmsg

    # Copy the Sample Code from the LRM file to LOT_CODE in tracking file, based on the identifier plot_plant in LRM and INTERNAL_CODE in tacking file
    ########################## CHANGED BY SHYULY 17-7-19 ############################################################
    #trac_df['LOT_CODE']=trac_df['INTERNAL_CODE'].apply(lambda x: lrm_df[lrm_df['plot_plant']==x]['Sample Code'].values[0] if x in lrm_df['plot_plant'].values.tolist() else 'EMPTY')
    trac_df['LOT_CODE']=trac_df['INTERNAL_CODE_GENOTYPE'].apply(lambda x: lrm_df[lrm_df['plot_plant_lotcode']==x]['Sample Code'].values[0] if x in lrm_df['plot_plant_lotcode'].values.tolist() else 'EMPTY')
    ########################## CHANGED BY SHYULY 17-7-19 ############################################################

    trac_df['GENOTYPE'] = trac_df['GENOTYPE'].str.replace('/','_')

    to_paste=trac_df
    to_paste.index=to_paste['PLATE_NAME']
    to_paste.drop('PLATE_NAME',axis=1,inplace=True)

    paste_data_to_xlsm_file(tracking_fname, trac_df, path)

def paste_data_to_xlsm_file0(tracking_fname, trac_df, path):
    shutil.copyfile(tracking_fname,'temp1.xlsm')

    wb = load_workbook(filename='temp1.xlsm', read_only=False, keep_vba=True,keep_links=True)
    #wb = load_workbook(tracking_fname)
    # #sheets = wb.sheetnames
    Sheet1 = wb['Samples_List']
    # #Then update as you want it
    # Sheet1.cell(row = 1, column = 1).value = "AMIT IS THE BEST" 
    from openpyxl.utils.dataframe import dataframe_to_rows
    rows = dataframe_to_rows(trac_df)

    for r_idx, row in enumerate(rows, 1):
        for c_idx, value in enumerate(row, 1):
            Sheet1.cell(row=r_idx, column=c_idx, value=value)
    wb.save("temp2.xlsm",) 

    if os.path.isdir('temp_1'):
        os.system('rm -r temp_1/')
    os.mkdir('temp_1')
    
    if os.path.isdir('temp_2'):
        os.system('rm -r temp_2/')
    os.mkdir('temp_2') 
    
    zip_ref = zipfile.ZipFile("temp1.xlsm", 'r')
    zip_ref.extractall('temp_1/')
    zip_ref.close()
    zip_ref = zipfile.ZipFile("temp2.xlsm", 'r')
    zip_ref.extractall('temp_2/')
    zip_ref.close()

    files=['/xl/worksheets/'+x for x in os.listdir('temp_2/xl/worksheets/') if x.endswith('.xml')]
    for f in files:
    #     os.remove('temp_2'+f)
        
        file = open('temp_2'+f, 'r') 
        temp=file.read() 
        file.close()
        
        lo='sheetData'
        s=temp.find(lo)-1
        e=temp.find(lo,temp.find(lo)+2)+10
        to_past=temp[s:e]
        
        file = open('temp_1'+f, 'r') 
        temp=file.read() 
        file.close()

        s=temp.find(lo)-1
        e=temp.find(lo,temp.find(lo)+2)+10
        to_rep=temp[s:e]    
        temp=temp.replace(to_rep,to_past)
        os.remove('temp_1'+f)
        file = open('temp_1'+f, 'w') 
        file.write(temp)
        file.close()
        
    shutil.copyfile('temp_2/xl/sharedStrings.xml','temp_1/xl/sharedStrings.xml')
    if os.path.isfile(path+'new_trac_file.xlsm'):
        os.remove(path+'new_trac_file.xlsm')
    shutil.make_archive(path+'new_trac_file.xlsm', 'zip', 'temp_1/')
    os.rename(path+'new_trac_file.xlsm.zip',path+'new_trac_file.xlsm')

    os.system('rm -r temp_1/')
    os.system('rm -r temp_2/')
    os.system('rm temp1.xlsm')
    os.system('rm temp2.xlsm')


def paste_data_to_xlsm_file(tracking_fname, trac_df, path):
    shutil.copyfile(tracking_fname,'temp1.xlsm')

    wb = load_workbook(filename='temp1.xlsm', read_only=False, keep_vba=True,keep_links=True)
    #wb = load_workbook(tracking_fname)
    # #sheets = wb.sheetnames
    Sheet1 = wb['Samples_List']
    # #Then update as you want it
    # Sheet1.cell(row = 1, column = 1).value = "AMIT IS THE BEST" 
    from openpyxl.utils.dataframe import dataframe_to_rows
    rows = dataframe_to_rows(trac_df)

    for r_idx, row in enumerate(rows, 1):
        for c_idx, value in enumerate(row, 1):
            Sheet1.cell(row=r_idx, column=c_idx, value=value)
    wb.save("temp2.xlsm",) 

    if os.path.isdir('temp_1'):
        os.system('rm -r temp_1/')
    os.mkdir('temp_1')

    if os.path.isdir('temp_2'):
        os.system('rm -r temp_2/')
    os.mkdir('temp_2') 

    zip_ref = zipfile.ZipFile("temp1.xlsm", 'r')
    zip_ref.extractall('temp_1/')
    zip_ref.close()
    zip_ref = zipfile.ZipFile("temp2.xlsm", 'r')
    zip_ref.extractall('temp_2/')
    zip_ref.close()

    files=['/xl/worksheets/'+x for x in os.listdir('temp_2/xl/worksheets/') if x.endswith('.xml')]
    for f in files:
    #     os.remove('temp_2'+f)
        
        file = open('temp_2'+f, 'r') 
        temp=file.read() 
        file.close()
        
        lo='sheetData'
        s=temp.find(lo)-1
        e=temp.find(lo,temp.find(lo)+2)+10
        to_past=temp[s:e]
        
        file = open('temp_1'+f, 'r') 
        temp=file.read() 
        file.close()

        s=temp.find(lo)-1
        e=temp.find(lo,temp.find(lo)+2)+10
        to_rep=temp[s:e]    
        temp=temp.replace(to_rep,to_past)
        os.remove('temp_1'+f)
        file = open('temp_1'+f, 'w') 
        file.write(temp)
        file.close()
        
    shutil.copyfile('temp_2/xl/sharedStrings.xml','temp_1/xl/sharedStrings.xml')

    if os.path.isfile(path+'new_trac_file.xlsm'):
        os.remove(path+'new_trac_file.xlsm')
    shutil.make_archive(path+'new_trac_file.xlsm', 'zip', 'temp_1/')
    os.rename(path+'new_trac_file.xlsm.zip',path+'new_trac_file.xlsm')

    #if os.path.isfile('my.xlsm'):
    #    os.remove('my.xlsm')
    #shutil.make_archive('my.xlsm', 'zip', 'temp_1/')
    #os.rename('my.xlsm.zip','my.xlsm')

    #os.system('rm -r temp_1/')
    #os.system('rm -r temp_2/')
    #os.system('rm temp1.xlsm')
    #os.system('rm temp2.xlsm')


@fileuploadBP.route('/get_error', methods=['GET'])
def get_error():
    global errormsg
    ret=errormsg
    errormsg=""
    return  ret



@fileuploadBP.route('/trac_to_lrm', methods=['GET', 'POST'])
def upload_file():
    global errormsg
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            errormsg = "Please choose a tracking file"
            return redirect('/')
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            errormsg = "Please choose a tracking file"
            return redirect('/')
            #return redirect(url_for('fileuploadBP.errors'))
        if file:
            # filename = secure_filename(file.filename)
            filename='infile.xlsm'
            file.save('/myapp/temp_folder/'+ filename)
        
            try:
                proc_tracking_2_LRM()
                errormsg=''
                return send_from_directory(directory='/myapp/temp_folder/', filename='outfile.xlsx',as_attachment=True,attachment_filename=file.filename[:-5]+'_LRM.xlsx' )    
            except Exception as e:
                errormsg=str(e)
               
            return redirect('/')      
            

@fileuploadBP.route('/lrm_to_trac', methods=['POST'])
def lrm_to_trac_file():
    global errormsg
    if request.method == 'POST':
        # check if the post request has the file part
        
        if 'lrm_file' in request.files:
            lrm_file = request.files['lrm_file']
            # if user does not select file, browser also submit an empty part without filename
            #if lrm_file.filename == '':
            #    errormsg += "Please choose an LRM file\n"
            #    #return redirect('/')
            #else:
            if lrm_file:
                # filename = secure_filename(file.filename)
                filename='lrm_file.xlsx'
                lrm_file.save('/myapp/temp_folder/'+ filename)
        else:
            errormsg = "Please choose an LRM file\n"
            #return redirect('/')

        if 'trac_file' in request.files:
            trac_file = request.files['trac_file']
            # if user does not select file, browser also submit an empty part without filename
            # if trac_file.filename == '':
            #     errormsg = "Please choose a Tracking file"
            #     return redirect('/')
            if trac_file:
                # filename = secure_filename(file.filename)
                filename='trac_file.xlsm'
                trac_file.save('/myapp/temp_folder/'+ filename)     
        else:
            errormsg += "Please choose a Tracking file\n"
            #return redirect('/')       

        if errormsg == '':   
            try:
                proc_lrm_2_tracking()  
                errormsg=''      
                return send_from_directory(directory='/myapp/temp_folder/', filename='new_trac_file.xlsm',as_attachment=True,attachment_filename='NEW_tracking_file.xlsm' )
            except Exception as e:
                errormsg=str(e)
               
        return redirect('/')     
        

#amit worked hard on it
def multiple_upload_save_me_for_later():
    
    for file in request.files.getlist('pro_attachment1'):

        # check if the post request has the file part
        # if 'file' not in request.files:
        #     flash('No file part')
        #     return redirect(request.url)
        # file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file:
            filename = secure_filename(file.filename)
            # filename='temp.xlsm'
            file.save('/myapp/temp_folder/'+ filename)
            #uploads = os.path.join(current_app.root_path, app.config['UPLOAD_FOLDER'])
    return "ok"       