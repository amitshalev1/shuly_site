from flask import Blueprint, request, jsonify, session
import json


USERS_FILE = 'data/users.json'

usersBP = Blueprint('usersBP', __name__)


#--------------------------- Helper functions --------------------------------
def save_users(users):
    '''function to push users into the USERS_FILE'''
    with open(USERS_FILE, 'w') as f:
            f.write(json.dumps(users))

def get_users():
    '''an helper function to load all users from USERS_FILE...'''
    try:
        with open(USERS_FILE) as f:
            return json.load(f)
    except:
        users = []
        save_users(users)
        return []

def get_next_user_id(users):
    '''helper function to return the max user_id+1'''
    ids = [int(u['user_id']) for u in users]
    if len(ids) == 0:
        return 0
    else:
        return max(ids)

def hash_password(password):
    assert len(password) > 0, 'No password provided!'
    import hashlib
    return hashlib.sha1(password.encode('utf-8')).hexdigest()

def user_exist(users, name):
    '''iterate trough users and check if the name exists'''
    if len([u for u in users if u['name'] == name])>0:
        return True
    return False 

def add_user(name, password):
    '''Adds a user and hashed password to the users database'''
    users = get_users()
    if not user_exist(users, name):
        users.append({'user_id' : get_next_user_id(users), 
                      'password': hash_password(password),
                      'name' : name,
                      'is_admin' : 0})
        save_users(users)
    return users

def login_check(name, password, users):
    password = hash_password(password)
    if password == [u for u in users if u['name'] == name][0]['password']:
        return 'login ok'
    else:
        return 'wrong password'


def user_register_or_login(name, password):
    users = get_users()
    if user_exist(users, name):
        return login_check(name, password, users)
    else:
        add_user(name, password)
        return '%s registered'%(name)


def delete_user(name):
    users = get_users()
    users = [u for u in users if u['name'] != name] # !!!
    save_users(users)
    return users

def check_admin_login():
    if 'current_user' in session:
        user = [u for u in get_users() if u['name'] == session['current_user']][0]
        print(user)
        if int(user.get('is_admin', '0')) != 0:
            return True
    return False

#============================================================================
#==================        Users API               ==========================
#============================================================================
        

@usersBP.route('/login_or_register', methods=['POST'])
def login_or_register():
    name = request.form['name']
    password = request.form['password']
    results = user_register_or_login(name, password)
    if not results == 'wrong password':
        session['current_user'] = name
    return results

@usersBP.route('/check_if_loginned', methods=['GET'])
def check_if_loginned():
    '''This API call will check if the current user is already logged in'''
    if 'current_user' in session:
        return session['current_user']
    else:
        return 'not logged in'

@usersBP.route('/user_logout', methods=['GET'])    
def user_logout():
    if 'current_user' in session:
        del session['current_user']
    return "logout"


@usersBP.route('/is_admin')    
def is_admin():
    if 'current_user' in session:
        user = [u for u in get_users() if u['name'] == session['current_user']][0]
        return str( user.get('is_admin', '0') )
    return '0'


@usersBP.route('/get_all_users') 
def get_all_users():
    users = get_users()
    if 'current_user' in session:
        user = [u for u in get_users() if u['name'] == session['current_user']][0]
        print(user)
        if int(user.get('is_admin', '0')) != 0:
            return jsonify(users)
        else:
            return 'NOT ADMIN!'


@usersBP.route('/delete_user_by_admin/<name>')     
def delete_user_by_admin(name):
    if check_admin_login():
        delete_user(name)
        return jsonify(get_users())
    else:
        return 'Not authorized!'