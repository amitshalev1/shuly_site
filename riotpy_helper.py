from riotpy import *


class general_functions(RiotTag):
    def JS(self):
        @make_self
        def map_to_inner(arr,inner_key_list):
            def map_arr(pat):
                i=0
                temp=pat
                while inner_key_list[i]!=undefined:
                    temp=temp[inner_key_list[i]]
                    i+=1
                return temp 


            return arr.map(map_arr) 

        @make_self
        def js_bool(a):
            if a:
                return 1
            else:
                return 0     

        @make_self
        def js_sum(a):
            summer=0
            for val in a:
                summer+=val
            return summer

        @make_self
        def sum_bool_array(a):   
            def map_fun1(pat):
                return me.js_bool(pat)
            
            return js_sum(a.map(map_fun1))                             
        @make_self
        def js_np_arange(list_len):  
            res=[]
            for i in range(list_len):
                res.append(i+1)
            return res

        @make_self
        def ajaxpost(data,url,on_success):
            me.temp={}
            def c1(data):
                console.log(data)
            def c2(data):
                me.parent.trigger(on_success,data) 
            jQuery.ajax({
                    url: url,
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    success: c2,
                    error: c1})

        @make_self
        def ajaxget(data,url,on_success,auth):
            me.temp={}
            authorizationToken=auth
            def bs(request):
                request.setRequestHeader("Authorization", "JWT "+authorizationToken)
            def c1(data):
                console.log(data)
            def c2(data):
                me.parent.trigger(on_success,data) 
            jQuery.ajax({
                    url: url,
                    type: 'GET',
                    data: JSON.stringify(data),
                    beforeSend:bs,
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    success: c2,
                    error: c1})


class amitmdselectinput(RiotTag):
    def HTML(self):
        with col('{opts.col_class} input-field'):
                with select(id='{opts.id}'):
                    option(value='', content="{opts.label}", disabled='', selected='', style='color:grey')()
                    option(each = "{item in items}", value = '{item}', content='{item}')() 
                label("{opts.label}")()

    def JS(self):
        me.items=opts.items
        jQuery('#'+opts.id).material_select()
        
        @on("mount")
        def mount():                         
            jQuery('#'+opts.id).on('change',on_change)  
        
        @make_self
        def on_change(e):
            me.to_placeholder(e.target.value)
        @make_self
        def to_placeholder(val):                
            me.parent[opts.place_holder]=val
            me.parent.update()
            me.parent.trigger(opts.to_trigger)
        @make_self
        def menu_update(vals):      
            me.items=vals
            me.update()
            me.parent.update()
            jQuery('select').material_select()

        @make_self
        def set_val(val):
            me.to_placeholder(val)
            jQuery('select').val(val)
            jQuery('select').material_select()


      

