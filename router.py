#implementing riot router...
from riotpy import *

class RiotRouter(RiotTag):
    def HTML(self):
        div(id = 'routing_point')()


    def JS(self):
        me.current_tag = null
        me.routes = {
            "" : self.opts.default_tag,
            "task" : "task_viewer",
            "explore_data" : "explore_data",
            "dataset_explorer" : "dataset_explorer",
            "dataset_feature_learner" : "dataset_feature_learner"
        }
        
        @make_self
        def show_login():
            me.parent.tags.navbar.show_login = 1
            me.parent.tags.navbar.update()

        @make_self
        def hide_login():
            me.parent.tags.navbar.show_login = 0
            me.parent.tags.navbar.update()


        def mount_it(tag, options):
            me.current_tag = riot.mount('#routing_point', tag.lower(), options)
            try:
                loginbox = me.parent.tags.navbar.tags.loginbox
                loginbox.check_if_admin(document.current_user)
            except:
                print("problem in checking if admin!")
            

        def when_route(address, options):
            if address in me.routes:
                tag = me.routes[address]
                mount_it(tag, {'options' : options})
                jQuery('select').material_select()
                jQuery('.collapsible').collapsible()
                jQuery(window).off('keypress')


        route(when_route)
        route.start(True)
        
        
        

              
        
        
        
        

        

