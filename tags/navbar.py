from riotpy import *

#=================================
class navbar(RiotTag):
    def HTML(self):
        with nav(style="margin-top:-20px;  height:75px; padding:7px 10px; background:{opts.base_color}"):
            with h4():
                with a(href=''): 
                    img(src='{opts.logo}', style='float:left; margin-right:8px; margin-top:5px;')()
                span('{opts.title}', style='float:left;margin-top:5px;')()
                span('', id='ProjectTitle', style='float:left;margin-top:5px; margin-left:15px;')()
                #loginBox(show='{show_login}')()
    def JS(self):
        me.show_login = 1

#=================================
class loginBox(RiotTag):
    def HTML(self):
        fontsize = 'font-size:14px;'
        with row(show = "{current_user == ''}",style="float:right; width:400px; margin-top:5px;"):
            with col('s3', style=fontsize):
                input(type='text', placeholder='username', style='margin-top:-10px; color:white;', id='username')()
            with col('s3', style=fontsize):
                input(type='password', placeholder='password', style='margin-top:-10px; color:white', id='password')()
            with col('s5', style=fontsize):
                a(css_class='btn green lighten-1', content="LOGIN/Register", onclick="{login_clicked}")()
        
        with row(show = "{current_user != ''}",style="float:right; width:200px; margin-top:5px;"):
            a(css_class='btn  red lighten-1 left', content='logout {current_user}', onclick='{logout}', style='padding:0px 10px;')()
    
    def JS(self):
        me.current_user = ''
        document.current_user = me.current_user

        @make_self
        def trigger_login():
            me.check_if_admin(me.current_user)
            if not me.parent.parent.tags.riotrouter.current_tag == undefined:
                current_tag = me.parent.parent.tags.riotrouter.current_tag[0]
                current_tag.trigger('login_event')

        @make_self
        def trigger_logout():
            current_tag = me.parent.parent.tags.riotrouter.current_tag[0]
            me.parent.parent.tags.riotrouter.update()
            current_tag.trigger('logout_event')

        @make_self
        def check_if_admin(current_user):
            if current_user!='':
                def is_admin(res):
                    if not  me.parent.parent.tags.riotrouter.current_tag == null:
                        current_tag = me.parent.parent.tags.riotrouter.current_tag[0]
                        current_tag.is_admin = res
                        current_tag.trigger('admin_logged_in')
                jQuery.get('is_admin' , is_admin)
                

        @on("mount")
        def mount():
            #check is a user is already registered..
            def is_logged_in(res):
                if res != 'not logged in':
                    me.current_user = res
                    document.current_user = me.current_user
                    me.update()
                    me.trigger_login()
                    
                else:
                   #return #remove if not debug mode!!!!   #----------------  WARNING !!!!! REMOVE IN PRODUCTION --------------------
                   me.logout()
                   me.update()
                   Materialize.toast('<h5>Please login or register!</h5>', 2000, 'red')  
            jQuery.get('check_if_loginned', is_logged_in)

            
            

        @make_self
        def validate_username_password(un, ps):
            valid = True
            if len(un) < 2:
                Materialize.toast('<h5>user name must contain at least 2 characters!</h5>', 5000, 'red')
                valid = False
            if len(ps) < 6:
                Materialize.toast('<h5>password must contain at least 6 characters!</h5>', 5000, 'red')
                valid = False
            return valid
            

        @make_self
        def login_clicked(e):
            un = jQuery('#username').val()
            ps = jQuery('#password').val()
            if validate_username_password(un, ps):
                data = {'name' : un, 'password': ps}
                def try_login(res):
                    if res == 'wrong password':
                        Materialize.toast('<h5>Wrong Password!</h5>', 3000, 'red')
                        me.current_user = ''
                        document.current_user = me.current_user
                    else:
                        me.current_user = jQuery('#username').val()
                        document.current_user = me.current_user
                        me.update()
                        Materialize.toast('<h5>'+res+'</h5>', 3000, 'green')
                        me.trigger_login()
                    
                jQuery.post('login_or_register', data, try_login)
        
        @make_self
        def logout(e):
            def on_logout(res):
                
                route('')
                me.current_user = ''
                
                current_tag = me.parent.parent.tags.riotrouter.current_tag[0]
                current_tag.is_admin = 0
                current_tag.trigger('logged_out')

                me.parent.parent.tags.riotrouter.update()
                me.is_admin = False
                document.current_user = me.current_user
                me.update()
                me.trigger_logout()
                
            jQuery.get('user_logout', on_logout)

