from riotpy import *
from riotpy_helper import general_functions


#==================================================
class tasks_tag(RiotTag):
    def HTML(self):
        general_functions()()
        h1(content='Convert Tracking to LRM')()
        with col('s2'):
            #file upload capability...
            with form(method='post' ,enctype="multipart/form-data"):
                input(type='file' ,name='file')()
                input(type='submit' ,name='Upload')()

    def CSS(self):
        css('#left_menu', height='100%', border_right='1px solid #777')
    
    def JS(self):
        @on("mount")
        def mount():
            me.update()

        @make_self
        def saveAs(blob, fileName):
            url = window.URL.createObjectURL(blob)
            anchorElem = document.createElement("a")
            anchorElem.style = "display: none"
            anchorElem.href = url
            anchorElem.download = fileName
            document.body.appendChild(anchorElem)
            anchorElem.click()
            document.body.removeChild(anchorElem)
            setTimeout(lambda x:  window.URL.revokeObjectURL(url), 1000)
   

        @make_self
        def file_selected(e):
            '''UPLOADING FILE (CSV)...'''
            fname = e.target.files[0].name
            console.log(fname)
            me.reader = FileReader()
            file_1=jQuery('#csv_file_upload_input').get(0).files[0] 
            me.reader.readAsText(file_1 )  
           
                 
            def file_loaded(res):

                # blob = Blob([me.reader.result], {'type': "application/octet-stream"})
                # #blob = Blob([me.reader.result], {'type': 'application/ms-excel'})
                # fileName = "myFileName.myExtension"
                # me.saveAs(blob, fileName)
                # temp=XLSX.read(dat1a, {'type':"array"})
                # temp1=me.to_csv(temp)
                # console.log(temp1)              
                def project_create_response(res):
                    console.log('res')
                    '''update the cards based on the projects...'''
                    # if res[0] == 'error':
                    #     print(res)
                    #     Materialize.toast('Error creating project!', 2000, 'red')
                    # else:
                    #     pcards = me.parent.tags.projectcards
                    #     pcards.projects = res
                    #     pcards.update()
                    #     Materialize.toast('project created', 2000, 'green')
                    #     me.cache_projects_data()
                jQuery.post('upload_excel', {'excelfile':me.reader.result}, project_create_response, 'json')                 
                Materialize.toast('Uploaded', 2000, 'green')
                jQuery('#csv_upload_label').removeClass('red')
                jQuery('#csv_upload_label').addClass('green')
                jQuery('#csv_upload_label_check').show()
            me.reader.addEventListener("load", file_loaded)         
            
    
